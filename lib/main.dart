import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Location'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _lat = 0;
  double _lang = 0;
  bool permission, _servicePermission;
  Location _location = new Location();
  LocationData currentLocation;
  var locality, countryCode;
  bool flag = false;

  @override
  void initState() async {
    super.initState();
    permission = await _location.requestPermission();
    if (permission == true) {
      _servicePermission = await _location.requestService();
      if (_servicePermission == true) {
        currentLocation = await _location.getLocation();
        setState(() {
          _lat = currentLocation.latitude;
          _lang = currentLocation.longitude;
        });
      } else {
        Toast.show("Please enable that service.", context);
      }
    } else {
      Toast.show("Please allow the permission.", context);
      flag = true;
    }
    if (currentLocation != null) {
      final coordinates = new Coordinates(_lat, _lang);
      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      var first = addresses.first;
      setState(() {
        countryCode = first.countryCode;
        locality = first.locality;
      });
    }
  }

  void _getLocation() async {
    try {
      _servicePermission = await _location.requestService();
      if (_servicePermission == true) {
        currentLocation = await _location.getLocation();
        print(currentLocation.latitude);
        print(currentLocation.longitude);
        setState(() {
          _lat = currentLocation.latitude;
          _lang = currentLocation.longitude;
        });
      } else {
        Toast.show("Please enable that service", context);
      }
    } on PlatformException {
      Toast.show("Please allow the permission.", context);
      flag = true;
    }
    if (currentLocation != null) {
      final coordinates = new Coordinates(_lat, _lang);
      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      var first = addresses.first;
      setState(() {
        countryCode = first.countryCode;
        locality = first.locality;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(child: currentLocation != null ? _customWidget() : _customProgress()),
      floatingActionButton: FloatingActionButton(
        onPressed: _getLocation,
        tooltip: 'Get City',
        child: Icon(Icons.location_city),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _customWidget() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Your current location is:',
          ),
          Text(
            'Latitude : $_lat, Longitude : $_lang',
            style: Theme.of(context).textTheme.subhead,
          ),
          Text(
            'Country Code : $countryCode & City Name : $locality',
            style: Theme.of(context).textTheme.subhead,
          ),
        ],
      );
  }
  _customProgress(){
    return CircularProgressIndicator(
      strokeWidth: 5.0,
      backgroundColor: Colors.lightBlueAccent,
    );
  }
}
